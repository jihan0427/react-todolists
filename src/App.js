import React, { Component } from 'react'
import Header from './components/Header'
import Lists from './components/Lists'
import Footer from './components/Footer'
export default class App extends Component {

  state = {
    lists: [
      {id: 1, name: '吃饭', done: false},
      {id: 2, name: '睡觉', done: true}
    ]
  }

  editItemDone = (id, status) => {
    const {lists} = this.state
    const newData = lists.map(reward => {
      if (reward.id === id) {
        reward.done = status
      }
      return reward
    })
    this.setState({ lists: newData })
  }

  delItem = (id) => {
    const {lists} = this.state
    const newData = lists.filter(reward => reward.id !== id)
    this.setState({ lists: newData })
  }

  addItem = (data) => {
    const {lists} = this.state
    const newData = [data, ...lists]
    this.setState({ lists: newData })
  }

  selectAll = (status) => {
    const {lists} = this.state
    const newData = lists.map(reward => {
      reward.done = status
      return reward
    })
    this.setState({ lists: newData })
  }

  clearDoneTrueItem = () => {
    const {lists} = this.state
    const newData = lists.filter(reward => !reward.done)
    this.setState({ lists: newData })
  }
  
  render() {
    const { lists } = this.state
    return (
      <div id="root">
        <div className="todo-container">
          <div className="todo-wrap">
            <Header addItem={this.addItem}/>
            <Lists lists={lists} editItemDone={this.editItemDone} delItem={this.delItem}/>
            <Footer lists={lists} selectAll={this.selectAll} clearDoneTrueItem={this.clearDoneTrueItem}/>
          </div>
        </div>
      </div>
    )
  }
}
