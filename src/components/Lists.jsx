import React, { Component } from 'react'
import Item from './Item'
class Lists extends Component {
  render () {
    const { lists, editItemDone, delItem } = this.props
    return (
      <ul className="todo-main">
        {
          lists.map(rewrad => {
            return <Item key={rewrad.id} data={rewrad} editItemDoneFunction={editItemDone} delItemFunction={delItem}/>
          })
        }
      </ul>
    )
  }
}

export default Lists