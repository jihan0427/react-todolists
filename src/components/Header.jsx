import React, { Component } from 'react'
import {nanoid} from 'nanoid'
class Header extends Component {


  addItem = (e) => {
    if(e.keyCode !== 13 || !e.target.value) return
    this.props.addItem({
      id: nanoid(),
      name: e.target.value,
      done: false
    })
    e.target.value = ''
  }


  render () {
    return (
      <div className="todo-header">
        <input type="text" placeholder="请输入你的任务名称，按回车键确认" onKeyUp={this.addItem}/>
      </div>
    )
  }
}

export default Header