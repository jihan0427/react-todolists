// import React, { Component } from 'react'

// class Item extends Component {

//   state = {
//     isSelect: false
//   }

//   editItemDone = (id, e) => {
//     this.props.editItemDone(id, e.target.checked)
//   }


//   select = (status) => {
//     this.setState({ isSelect: status })
//   }

//   delItem = (id) => {
//     if(window.confirm('确定删除吗?'))
//     this.props.delItem(id)
//   }


//   render () {
//     const {data} = this.props
//     const {isSelect} = this.state
//     return (
//       <li onMouseEnter={() => this.select(true)} onMouseLeave={() => this.select(false)} style={{ backgroundColor: isSelect ? '#eee' : ''  }}>
//         <label>
//           <input type="checkbox" checked={data.done} onChange={(e) => this.editItemDone(data.id, e)}/>
//           <span>{data.name}</span>
//         </label>
//         <button className="btn btn-danger" style={{ display: isSelect ? 'block' : 'none' }} onClick={() => this.delItem(data.id)}>删除</button>
//       </li>
//     )
//   }
// }

// export default Item


import React, {useState, useEffect} from 'react'

const Item = (props) => {
  const {data, editItemDoneFunction, delItemFunction} = props
  const [select, setSelect] = useState(false)

  useEffect(() => {
    console.log('我输出了');
  }, [select])

  const editItemDone = (id, e) => {
    editItemDoneFunction(id, e.target.checked)
  }

  const delItem = (id) => {
    if(window.confirm('确定删除吗'))
    delItemFunction(id)
  }

  const selectFunction = (status) => {
    setSelect(status)
  }

  return (
    <li onMouseLeave={() => selectFunction(false)} onMouseEnter={() => selectFunction(true)} style={{ backgroundColor: select ? '#eee' : ''  }}>
      <label>
        <input type="checkbox" checked={data.done} onChange={(e) => editItemDone(data.id, e)}/>
        <span>{data.name}</span>
      </label>
      <button className="btn btn-danger" onClick={() => delItem(data.id)} style={{ display: select ? 'block' : 'none' }}>删除</button>
    </li>
  )
}

export default Item