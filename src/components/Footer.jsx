import React, { Component } from 'react'

class Footer extends Component {

  selectAll = (e) => {
    this.props.selectAll(e.target.checked)
  }

  clearDoneTrueItem = () => {
    this.props.clearDoneTrueItem()
  }



  render () {
    const {lists} = this.props
    const doneTrueLength = lists.filter(reward => reward.done).length
    return (
      <div className="todo-footer">
        <label>
          <input type="checkbox" checked={doneTrueLength === lists.length && lists.length > 0 ? true : false} onChange={this.selectAll}/>
        </label>
        <span>
          <span>已完成{doneTrueLength}</span> / 全部{lists.length}
        </span>
        <button className="btn btn-danger" onClick={this.clearDoneTrueItem}>清除已完成任务</button>
      </div>
    )
  }
}

export default Footer